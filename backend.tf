terraform {
  backend "gcs" {
    bucket  = "bucket1-store-tfile" #BUCKET IS ALREADY CREATED IN GCP
    prefix  = "terraform/TFstateFILE"
    credentials = "keyfile.json"
  }
}
